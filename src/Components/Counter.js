import React from 'react';
import { connect } from 'react-redux';
import background from '../background.jpg';

class Counter extends React.Component {
  increment = () => {
    this.props.dispatch({ type: 'INCREMENT' });
  }

  decrement = () => {
    this.props.dispatch({ type: 'DECREMENT' });
  }

  reset = () => {
    this.props.dispatch({ type: 'RESET'});
  }

  render() {
    return (
      <div className="container">
        <img src={ background } alt="Background" />
        <div className="counter-container">
          <div className="content">
            <p>COUNTER</p>
              <span>{this.props.count}</span>
            <div className="counter">
              <button onClick={this.decrement}>-</button>
              <button onClick={this.reset}>Reset</button>
              <button onClick={this.increment}>+</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
      count: state.count
    };
}  

export default connect(mapStateToProps)(Counter);