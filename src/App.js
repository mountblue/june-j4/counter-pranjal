import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Counter from './Components/Counter';

// const initialState = {
//   count: 0
// };

// function reducer(state = initialState, action) {

function reducer(state, action) {
  switch(action.type) {
    case 'INCREMENT':
      return {
        count: state.count + 1
      };
    case 'DECREMENT':
      return {
        count: state.count - 1
      };
    case 'RESET':
      return {
        count: 0
      };
    default:
      return state;
  }
}

const persistedState = localStorage.getItem('counterValue') ? JSON.parse(localStorage.getItem('counterValue')) : 0;

const store = createStore(reducer, persistedState);

store.subscribe(() => {
  localStorage.setItem('counterValue', JSON.stringify(store.getState()));
});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
       <Counter />
      </Provider>
    );
  }
}

export default App;
